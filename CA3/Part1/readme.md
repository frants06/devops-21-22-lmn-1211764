# Class Assignment 3 Part 1
## Introduction
 In this assignment we are meant to explore virtualization. The hypervisor we're using is VirtualBox VM with Ubuntu.
 The goal is to run the projects from the previous assignments but now inside a Virtual Machine.
"Virtualization is a technique allowing computers to emulate other computers as well as various hardware devices." (Quote from class documentation)

A hypervisor is a software that creates and runs virtual machines. A hypervisor allows on host computer to support multiple guest Vms by sharing its resources, such as memory and processing.

There is two types of virtualization: hardware-level and hosted.

### Analysis, design and implementation of the requirements
##### Creating a virtual machine with VirtualBox
**Step One:** After opening the software click on the new button and choose the desired settings.

![Creating a new VM](StepOneA.JPG)

![Basic Settings](StepOneB.JPG)

![RAM](StepOneC.JPG)

Notes: On the type select Linux since we're using Ubuntu, and we need at least 2048 MB of RAM to run this VM.

**Step Two:** After creating the VM, we need to set a second Network Adapter as a Host-only Adapter in order to be able to connect the VM with the host.
Go to _settings_, Network and enable the second adapter as a Host-only Adapter.
Then, go to _files_ and select _Host Network Manager_ and create a new one.

![Enabling a second Adapter](Step2A.JPG)

![Creating new network Adapter](Step2B.JPG)

Notes: We should take a note of the IP address of this network because it is going to be important when we have Ubuntu installed.

**Step Three:** If we attempt to start the Virtual Machine, a message like this on the console will pop out:

![No ISO](Step3A.JPG)

This will appear because we haven't connected the iso with the Ubuntu minimal installation to the VM's virtual CD reader.
In order to do it we need to go to Settings once again, go to the Storage tab and click on the small disk.

![Basic Settings](Step3B.JPG)

Note: The Live CD/DVD option should be enabled.

Now if we attempt to start the VM again, an installation window for Ubuntu will pop up.

![Installing Ubuntu](Step3C.JPG)

**Step Four:** After installing Ubuntu, the first thing we should do is set up the IP address of the second adapter within the range of the network.
For that we should type the following commands on the console:

`sudo apt update
sudo apt install net-tools
sudo nano /etc/netplan/01-netcfg.yaml`

And edit the file with the following configuration.

     `network:
       version: 2
       renderer: networkd
       ethernets:
         enp0s3:
           dhcp4: yes
         enp0s8:
           addresses:
            - 192.168.56.5/24`

Notes: Tab Indentation are not allowed.

The use the command `sudo netplan apply` to apply our changes.

Other important options we have is _openssh-serve_ so that we can use ssh to open secure terminal sessions to the VM from other hosts.
We also can install a ftp server so that we can use the FTP protocol to transfer files between the host and the VM.

Step Five: Now that we have finished installing and configuring the VM, we can attempt to run the projects from the previous
assignments. The process should be quite similar to what we're used to, with one major difference. Since ubuntu doesn't have
a GUI we need to use the browser in our host to access web applications (e.g.:the spring and react tutorial app), and we also need
to use the host to run the client for the simple chat application for the same reason.

After making sure we installed every dependency the projects have on the VM, such as git for instance, firstly, we need to clone our
repository to the VM. 

Then to run the sprint and react tutorial app, we need to use the command `cd` to change to the _basic_ folder of the app, then run the
command `./mvnw springboot:run` and then open the app on the host's browser with th URL _http://192.168.56.5:8080/_ instead of
_http://localhost:8080_ like we're used to, because we're running the app from the WM, so we need to use its IP address we've set up earlier.

With the simple chat app, it is quite similar, but we also have to make a change on the host's build.gradle file, on the runClient task to match the right IP
address. After changing our location to where the app is in our repository using once again the command `cd`, we should run the client on the VM using the command `./gradlew runServer` and then, on the host `./gradlew runClient` and it should work.

### The Alternative - Research

The alternative I explored for this assignment was VMWare Workstation. Throughout the completion of the assignment I didn't find
any major differences. The biggest difference I found doing research was that VMWare doesn't support Software virtualization. 
The GUI is obviously different, but the process is very intuitive as we worked with another hypervisor
before. 

### The Alternative - Implementation

As soon as we start creating the virtual machine, the software allows us to connect with the iso image. 

![VMWare GUI](AltStep1A.JPG)

![Connecting ISO](AltStep1B.JPG)

Setting up the Host-only adapter was also quite similar. The only difference is that I didn't find a way to check the IP within the software like we did on VirtualBox
so, I had to use the command `ipconfig` on the host terminal to check it. 

![Host only adapter](AltStep2A.JPG)

After installing Ubuntu, the process of running both applications was exactly the same with one slight difference. On the _netplan_
file, instead of "enp0s8", the name was "ens34".

Finally, after that and cloning the repo into the VM, just had to run the app on the VM and then access it on the host just like we did with VirtualBox.