# Class Assignment 3 Part 2
## Introduction
 The goal of the second part of this assignment is to explore vagrant to manage multiple VMs to execute the spring and react tutorial
 app. One is meant to be used for the database and the other to run tomcat and the basic app.
 
Vagrant is a tool to automate the set-up of one or more multiple virtual machines and can be very helpful because allows the 
user to define all configuration in one single file.

### Analysis, design and implementation of the requirements

Step one: After choosing the location of your project, in order to create a Vagrant project use the command:

`vagrant init envimation/ubuntu-xenial`

This command, will generate the _vagrantfile_ where we will define all our configurations to manage both VMs.

Note: If we wanted to add a box to our catalog without creating the vagrantfile we could have used the command:

`vagrant box add envimation/ubuntu-xenial`

Note: "Boxes are the package format for Vagrant environments. 
A box can be used by anyone on any platform that Vagrant supports to bring up an identical working environment."
([Source](https://www.vagrantup.com/docs/boxes))

 Each box has a provider, the hypervisor that can run it, the one we're using is a standard Ubuntu 16.04 LTS, specifically 
for VirtualBox.

Step two: To create two virtual machines, on the _vagrantfile_ we need to write the following lines:

    Vagrant.configure("2") do |config|
      config.vm.box = "envimation/ubuntu-xenial"

Something that can be very useful is to give a name to the VM alongside its private network IP address:

     config.vm.define "db" do |db|
     db.vm.box = "envimation/ubuntu-xenial"
     db.vm.hostname = "db"
     db.vm.network "private_network", ip: "192.168.56.11"

Later on, for instance, if we want to connect to our server via _ssh_ all we need to type on the command line is:

`vagrant ssh db`

Step three: We can use the _shell provisioner_ in the vagrantfile to execute a script with the guest machine.

There is the option of using `config.vm.provision "shell"`,to execute scripts that are common for both virtual machines
(e.g.: `sudo apt-get install openjdk-8-jdk-headless -y`, to install java).

On the other hand, we can also replace the `config` with the name of the VM to run a script specific to it:

    db.vm.provision "shell", inline: <<-SHELL
    wget https://repo1.maven.org/maven2/com/h2database/h2/1.4.200/h2-1.4.200.jar
    SHELL

    web.vm.provision "shell", inline: <<-SHELL, privileged: false
    git clone https://FranTS06@bitbucket.org/frants06/devops-21-22-lmn-1211764.git
    cd devops-21-22-lmn-1211764/CA3/Part2/tut-basic-gradle
    chmod u+x gradlew
    ./gradlew clean build

This last one is quite important, what it means is that once we boot the VMs using `vagrant up`, vagrant will clone our repository
automatically to our second VM, and run the application which we can then access on our browser(either app our database) by typing 
the url with the ip and port we defined on the _vagrantfile_. 

Step four: Once we have everything we need on our _vagrantfile_, all need to do is use `vagrant up` and the virtual machines
should be created and started.

If at anytime, we want to shut down our VMs, we just need to use the command, `vagrant halt`.

If we want to delete our the VMs instead, we need to use the command `vagrant destroy`. This however won't delete either the
vagrantfile or the boxes we download. For that use instead, `vagrant box remove NAME`

Important note: If at any time, we make any change in the provision are, to our _vagrantfile_ after executing `vagrant up` we need to use the command,
`vagrant reload --provision` in order to apply those changes.

### The Alternative - Research

The alternative I explored for this assignment was VMWare Workstation. Throughout the completion of the assignment I didn't find
any major differences. In order to support VMware, I had to install a vagrant plugin and also had to find a box with the right provider.

### The Alternative - Implementation

Creating a Vagrant environment using VMware to run the spring tutorial App was quite similar to Virtual box.

There were three keys differences:

1) I had to install a plugin to support VMware, first on the command line, had to run the command, `vagrant plugin install vagrant-vmware-desktop` and then download and install
the [VMware utility](https://www.vagrantup.com/docs/providers/vmware/vagrant-vmware-utility)
2) Other problem was that the box we previously used doesn't come with a VMware provider. Therefore I had to find another one
with the same version of linux but with right [provider](https://app.vagrantup.com/bento/boxes/ubuntu-16.04).

Also had to update it on the _vagrantfile_:

    Vagrant.configure("2") do |config|
    config.vm.box = "bento/ubuntu-16.04"

3)Update the IP address both on the _vagrantfile_ and on the _application.proprieties_ to one that is within the range of
the VMware Host Only network adapter:

    db.vm.network "private_network", ip: "192.168.146.13"
    web.vm.network "private_network", ip: "192.168.146.12"



