# Class Assignment 1
## Introduction
This file contains the technical report for the first assignment where we were challenged to explore **Git, a version control system**. 


## Sections

This report is going to be divided into three section as suggested by the assignment requirements:

1) A section dedicated to the description of the analysis, design and implementation of
   the requirements
- Should follow a "tutorial" style (i.e., it should be possible to reproduce the assignment by following the instructions in the tutorial).
- Should include a description of the steps used to achieve the requirements.
- Should include justifications for the options (when required)
2) A section dedicated to the analysis of the alternative
3) A section dedicated to the implementation of the alternative

Then, clone the repository and copy the appropriate template.

### Section 1
### Week 1, no branches

Step one: On BitBucket, create a new repository.

Step two: Clone the repository to the local machine -> Copy the repository's address. Open the command line. On my case I used gitBash. 
Choose the location in your computer where you want to clone the repository using the command `cd`. If necessary, 
create a new directory using the command `mkdir`. And finally use the command `git clone <repositoryAddress>`

Alternately, we can use the command `git innit` to start a git repository locally and then use the command `git push <bitBucketAddress>` to push it to the remote server.

Step three: Create a new folder for the first assignment. Make sure that on the command line you are in the right 
directory. If not, use again the command `cd`. Then, use command `mkdir`. If you use the command git status there will
be a message saying that the branch is up-to-date, there are no commits to-do.

`$ git status` <br> 
`On branch master`
<br> `Your branch is up-to-date with 'origin/master'.
nothing to commit, working tree clean`

This happens, because git doesn't recognize
new empty folders to be added to the Staging Area. If, for instance, we create the file "readme.md" in the new folder using 
the command `touch` and use the command `git status` again, then the new directory and file will appear as untracked. 

`$ git status`
<br>`On branch master`
<br>`Your branch is up-to-date with 'origin/master'.`
<br> `Untracked files:
(use "git add <file>..." to include in what will be committed)
    readme.md`

Use command `git add .` to add all new files to the Staging Area. 

`$ git status`
<br>`On branch master`
<br>`Your branch is up-to-date with 'origin/master'.`
<br> `Changes to be committed:
(use "git restore --staged <file>..." to unstage)`

    new file:   README`

Use command `git commit -m <commit message>` 
to then capture a snapshot of the project's currently staged changes. And finally use the command `git push` to send the updated version to the 
remote server on BitBucket. In order to copy the Tutorial React.js and Spring Data REST app into the repository, follow the same process again.

Step four: Tags. It is possible to tag each commit, for example to indicate the version of the repository. In order to do it, after the commit, use the command
`git tag <tagName>` e.g.: `git tag v.1.1.0`. By default, the command `git push` doesn't transfer the tags 
to the remote server. To do that, use the command `git push origin <tagName>`

In case there is a human error in the name of the tag or the attribution of the wrong tag to a commit it is possible to 
delete a tag. To delete in the remote server use the command `git push origin --delete <tagName>`. However, if you want
to delete the tag locally, use the command `git tag -d <tagName>`

Step five: Adding a new feature to the app. To add the new 'emailField' to the app it is necessary to edit the relevant files
in the basic folder of the tutorial app. You can to do it on the command line using an editor such as vim or nano. However, 
as the focus of the assignment is to explore git, for simplification I used Intellij.

Step six: After the new feature is implemented, tested and debugged, we need to repeat the process of staging the untracked files, 
committing those changes, then pushing them to the remote server on BitBucket and finally tagging the commit if necessary. We are working individually on this assignment
which means that while we were working on the new feature, no further changes were pushed into the server. However, if it 
was not the case, we could use the command `git pull` to automatically fetch and then merge that remote branch into our
current branch.

**Relevant remarks about the *git commit* command:**

`git commit --amend -m "an updated commit message"`

The amend option allows to change the message of the most recent commit.

`git rebase -i HEAD~n`

The rebase option allows we to put together the last *n* commits into one.

Note: This changes will only apply to the local repository. 

`git revert <commit>`

The git revert command is used for undoing changes to a repository's commit history. Other 'undo' commands like, git 
checkout and git reset , move the HEAD and branch ref pointers to a specified commit. Git revert also takes a specified commit, however, git revert does not move ref pointers to this commit.

Throughout the whole process we can use the command `git log` to retrieve a history of every commit that happened on the repository and its 
respective information like, checksum(an unique forty digit hash code to identify each commit), author, date, message and tag.

There are several combination to the log command, one of the most popular is `git log --oneline` that list every commit with summarized information.

    $ git log --oneline
    33f8929 (HEAD -> master, origin/master, origin/HEAD) solves merging conflict
    46318d5 (email-field) Testing Branches Merge conflict 2
    bcc2518 Testing Merge Conflict
    b618b96 (tag: v1.3.1, fix-invalid-email) Adds validation for email. Resolves #7
    6d92ad1 (tag: v1.3.0) CA1 Week 2: Adds email field
    9f88ac7 (origin/email-field) Updates read.me file with first steps of week one
    ffee323 (tag: v1.2.1, tag: ca1-part1) CA1: Adds validations for employee fields and unit tests. Resolves #2 #4 #5
    d746cc0 (tag: v1.2.0) CA1: Adds new field, Job Years. Solves Issue #3
    4310f69 (tag: v1.1.0) CA1: Added Tutorial React.js and Spring Data REST Application into first assignment folder
    38da399 (tag: v1.0.0) Create CA1 folder with readme file
    9eddbd2 added employee
    c4f4304 teste
    cfc7ca6 added getter and setter to jobTitle
    a292966 Added jobTitleSection on Frontend
    c2f7efe add jobTitleSection
    cb501a4 Changed to original settings
    d9d1b8a primeiro exercicio
    b83d960 Initial commit



### Section 1
### Week 2, branches

Step one: Creating a branch -> To create a branch use the command `git branch <branchName>`

You can check if the branch was successfully created by using the command `git branch` without anything after.
Like the tags if you want to push the branch to the remote server we need to use the command `git push origin <branchName>`

Step two: Changing branches -> To change branches use the command `git checkout <branchName>`. Now, we can start working 
on the project, without affecting the master branch that should only have code that compiles. Also, this is very beneficial
for team projects, since the probability of work getting lost decreases if every developer 
don't work directly on the master branch. Organizing works becomes easier. If, for instance, we make some changes
on the new branch, and then use the command `git status` we will be able to see that there are untracked files. If we go back
to the master branch using once again the command `git checkout master` and then `git status` there won't be any untracked 
files, since it reverts the files in our working directory back to the snapshot that master points to.

Step three: Merging the branches -> The second week exercise is very similar to the previous one in terms what it demands.
We need to add to the app an email field to the employee. The only difference is that by the time  we commit our changes(after
implementing, testing and debugging our new feature that we are developing on the new branch)
we can't simply push our changes since we are working on different branches. Instead, we need to merge the new branch with the
master branch. In order to do this we need to use the command `git checkout master` to return to the master branch again. Then,
use the command `git merge <branchName>` and then if there are no conflicts we can push our updates. Because this is an individual repository there won't be any conflicts because while
we were working on the new feature, obviously no one else made any changes on the master branch. However, for demonstration 
purposes I tested this by making some changes on the new branch created to add the email field to the employee, committed those changes, then went back to master
branch, made some changes on the same class I made changes on the other branch, purposely to force a conflict. Then when I tried to merge
it wasn't possible since there were simultaneous changes on the same file. To solve this we have to open our text editor
of choice, and then both versions of the sections where the are differences on the file from each branch will appear separated by a dashed line, 
so we can choose what we want to use and what we want to delete.

By the end of the merge we can delete the branch using the command `git branch -d <branchName>`, to delete locally and
the command `git push origin --delete <branchName>` to delete remotely.


### Section 2

The alternative I chose to analyse is **Mercurial**. Just like Git, Mercurial is a distributed version control system which allow the developer to clone a repository
down to their workstations.
From the research I did it appears that there are three main points that differentiate both:

-**Simplicity**

There are reports that Mercurial is considered simpler and faster. For one hand, Mercurial seem to have a less complicated
commands and an easy-to-understand GUI. For the other hand, even though, Git may require a bigger level of 
expertise, once you surpass the learning phase it may offer the user and the team more flexibility.
Mercurial doesn't support an index or a staging area like Git.


-**Immutability** 

Git allows the user to more easily manipulate the commit history. On the hand it appears that there is a culture of immutability
surrounding Mercurial. By default, you cannot manipulate history. If you commit something, it is going to be there. It is a stricter
work ethic that may be appreciated by some users.

-**Branching structure**

A branch in git is simply a new pointer to a specific commit which make them more lightweight and easier to use. 
Mercurial embeds the branches in the commits, where they are stored forever. 
This means that branches cannot be removed because that would alter the history.

### Section 3

Using mercurial to do the first week's assignment ended up being quite similar to using git. If we follow the first section's
tutorial but replace the word **git** in the command line with **hg** we are almost able to do the assignment.

The only key diferences were that there is no staging area which means the command `hg add .` is used to add new files to the 
next commit. Other difference that I found is that if we remove any files locally and then commit and push, the file will
still be at the remote server. To delete the file from the remote repository, firstly, before the commit we need to use
the command `hg addremove .` to add new files and remove the missing ones.

The final difference regarding the first part of the exercise where we don't use branches is about the tags. Whilst with 
**Git** when you create a tag, if you want to push them to the remote server you need to use the command `git push origin <tagName>`,
with **Mercurial** the creation of the tag with treated as a whole separate and new commit that you then simply push to the
remote repository using the command `hg push`.

Regarding the second part of the assignment using branches it was slightly different. Firstly, when we create a branch using
the command `hg branch <branchName>` we are immediately set on the new branch. There are no `checkout` commands with Mercurial.

`$ hg branch emailFix`
<br> `marked working directory as branch emailFix`
<br>`(branches are permanent and global, did you want a bookmark?)`

Then we need to use the command `hg update default` to switch to the "master" branch and finally, similarly to git use the command `hg merge <branchName>`

It is also possible to use Mercurial bookmarks that can have a similar effect to Git's branches.

As bitBucket doesn't support Mercurial any longer I used a
*codebasehq* [remote repository](https://isep.codebasehq.com/projects/ca1-alternative/repositories/alternative/tree/tip)

