package com.greglturnquist.payroll;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.util.Assert;

class EmployeeTest {
    @Test
    void createEmployeeFailureNegativeAge() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Joao", "Mario", "CTO", -1, "joao@isep.com");
        });

        Assertions.assertEquals("Invalid Employee - negative number", thrown.getMessage());
    }

    @Test
    void createEmployeeFailureEmptyFirstName() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("", "Mario", "CTO", 0, "joao@isep.com");
        });

        Assertions.assertEquals("Invalid Employee - one of the fields is empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailureEmptyLastName() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Joao", "", "CTO", 0, "joao@isep.com");
        });

        Assertions.assertEquals("Invalid Employee - one of the fields is empty", thrown.getMessage());
    }

    @Test
    void createEmployeeFailureEmptyDescription() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Joao", "Mario", "", 0, "joao@isep.com");
        });

        Assertions.assertEquals("Invalid Employee - one of the fields is empty", thrown.getMessage());
    }
    @Test
    void createEmployeeFailureEmptyEmail() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Joao", "Mario", "CTO", 0, "");
        });

        Assertions.assertEquals("Invalid Employee - one of the fields is empty", thrown.getMessage());
    }
    @Test
    void createEmployeeFailureInvalidEmail() {
        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class, () -> {
            Employee employee = new Employee("Joao", "Mario", "CTO", 0, "joaoisep.com");
        });

        Assertions.assertEquals("Invalid email", thrown.getMessage());
    }
    @Test
    void createUserSuccessful(){
        Employee employee = new Employee("Joao", "Mario", "CTO", 0, "joao@isep.com");

        Assertions.assertNotNull(employee);
    }
}
