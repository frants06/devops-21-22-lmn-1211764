# Class Assignment 4 Part 1
## Introduction
 The goal of this assignment was to create a docker image to run on a container with the right specifications to run the gradle chat app from class 2 assignment.

Docker is a platform that allows developers to package applications into containers - "standardized executable components combining application source code with the operating system (OS) libraries and dependencies required to run that code in any environment."
[Source](https://www.ibm.com/in-en/cloud/learn/docker)


### Analysis, design and implementation of the requirements
For this assignment we mainly use Dockerfile, a functionality that allows us to automate the process of setting up our image and container.
However, it is not necessary to use Dockerfile to create an image or a container. For that, all we need to do after installing docker on our machine is use the command:

`docker run --name [container name] -i -t [image] [command] [arguments]`

Basic docker commands:

`docker pull	 `   Pull an image or a repository from a registry

`docker push	 `   Push an image or a repository to a registry

`docker commit` 	Create a new image from a container’s changes

`docker images`	    List images

`docker create`	    Create a new container

`docker ps	 `       List containers

`docker run	 `      Start a new container

`docker exec	 `   Run a command in a running container


**Step one**: On the command line, choose the location where you want to create your Dockerfile and do `touch Dockerfile`.

**Step two**: After opening the Dockerfile on a text editor, the first thing is choosing the image we're using with the command `FROM`

The [FROM](https://docs.docker.com/engine/reference/builder/#from) instruction initializes a new build stage and sets the Base Image for subsequent instructions.

`FROM ubuntu:18.04`

**Step three**: Then we need to use the command `RUN` to install all dependencies we need like a version of Java or git if we intend to clone
our repository.

The [RUN](https://docs.docker.com/engine/reference/builder/#run) instruction will execute any commands in a new layer on top of the current image and commit the results.

    RUN apt update
    
    RUN apt install openjdk-8-jdk openjdk-8-jre-headless -y
    
    RUN mkdir -p /app

As we can see above, I also used the RUN command to create a directory where we will be copying our app to.

**Step four**: Next, we need to use the command `COPY` to copy the chat app from our host to the filesystem of the container. The files need to be in the same level as the Dockerfile.

Note: The directory itself is not copied, just its contents.

    COPY ./gradle_basic_demo app

**Step five**: Then, we need to use the command `WORKDIR` to establish where we will be working. In our case, it will be the app directory we created

The [WORKDIR](https://docs.docker.com/engine/reference/builder/#workdir) instruction sets the working directory for any RUN, CMD, ENTRYPOINT, COPY and ADD instructions that follow it in the Dockerfile

    WORKDIR /app

**Step six**: Use RUN again to execute the following commands:

    RUN chmod u+x gradlew
    
    RUN ./gradlew build

This will allow the container to build the app.

**Step seven**: Use the command `EXPOSE` to inform what port of the localhost, the container listens at runtime during the execution of the server.

`EXPOSE 59001`

**Step eight**: And finally use the command `CMD` to add a default instruction for when executing the container. In order case, to run the server of the chat app.

The main purpose of a [CMD](https://docs.docker.com/engine/reference/builder/#cmd) is to provide defaults for an executing container.

**Final Step**: Open the command line where we have the Dockerfile and use the command:

`docker build -t <give_a_tag_name> .`

This will create an image following the instructions of the Dockerfile. (We can check it using the command docker images)

Then, create and run a container using the image created with the command `docker run <image_id or tag>`

This will execute the server of the app. For the last step, all we need to do is run the client on the host.
 
We also had to repeat the whole process to create a container with only the _.jar_ file instead of the whole project.

The process is very similar, just needed to change the COPY and remove the execution of `./gradlew build`


    FROM ubuntu:18.04
    
    RUN apt update
    
    RUN apt install openjdk-8-jdk openjdk-8-jre-headless -y
    
    COPY basic_demo-0.1.0.jar .
    
    EXPOSE 59001
    
    CMD ./gradlew runServer

In the end we also add to publish the images on [docker hub](https://hub.docker.com/repository/docker/francisco1211764/devops-21-22-lmn-1211764), using the command:

    docker push <imageName>:<tagName>



