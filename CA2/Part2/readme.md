# Class Assignment 2 Part 2
## Introduction
 In this assignment we are meant to use gradle as the build tool for the _tut-react-and-spring-data-rest_ app.
 Just like last week's assignment, most of the work happened on the _build.gradle_ file except for setting up the
project that required configuration in other places.


### Analysis, design and implementation of the requirements
**Step one:** After generating and setting up a Gradle Spring project and replacing the source folder with the one from the
_tut-react-and-spring-data-rest, the first modification to the _build.gradle_ file we had to do was adding a plugin 
for the frontend to work. We added the following block of code to the plugin section and the respective configuration:

`id "org.siouan.frontend-jdk11" version "6.0.0"`

    frontend {
      nodeVersion = "14.17.3"
      assembleScript = "run build"
      cleanScript = "run clean"
      checkScript = "run check"
    }

In addition, we also had to update the scripts section on the _package.json_ file by adding the following code:

    "scripts": {
      "webpack": "webpack",
      "build": "npm run webpack",
      "check": "echo Checking frontend",
      "clean": "echo Cleaning frontend",
      "lint": "echo Linting frontend",
      "test": "echo Testing frontend"
    }

**Step two:** Add a task to gradle to copy the generated jar to a folder named "dist": This task is identical to one copy task
we did on the last week's assignment, the one thing we add to change was the "from" directory to the one where the jar files
are located.

    task copyJar(type:Copy){
      from 'build/libs'
      into 'dist'
    }

**Step two:** Add a task to gradle to delete all the files generated by webpack. This new task should be executed automatically
by gradle before the task clean. This one adds another layer of complexity because we have to add another layer of complexity,
add dependencies between tasks. First, I create a task to delete the files by using a _Delete_ task type. Then to implement the
task we just need to write _delete_ following with the directory where the files to be deleted are located.

    task delete(type: Delete){
		delete "src/main/resources/static/built/."

    }

Then, we need to write the following line of code, to make sure that when we run the clean task on the command line: `./gradlew clean`, 
first the _delete_ task we created will run.

`clean.dependsOn delete`

### Analysis, design and implementation of the requirements
 
Maven is a very popular alternative to Gradle. The main difference I found is that Maven uses _xml_ instead of a programming
language which makes it less flexible than Gradle. This means there are no such things as _"Tasks"_ on Maven. The alternative
I found in order to create the "task" to copy and other to delete some files was use some plugins that have the same purpose

####Copy the generated jar file
       
            <plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.2.0</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<!-- here the phase you need -->
						<phase>validate</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${basedir}/dist</outputDirectory>
							<resources>
								<resource>
									<directory>/target</directory>
									<include>
										react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.jar
									</include>
									<filtering>true</filtering>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

Note: I could find the generated jar files on the Maven project, so for demonstration purposes I planted the file on 
the Target directory.

After configuring the plugin, and as we can see it is more extensive than the Gradle task, we just need to run on the
command line:

`./mvnw processes-resource`

####Delete the files generated by the webpack

      <plugin>
				<artifactId>maven-clean-plugin</artifactId>
				<version>3.2.0</version>
				<configuration>
					<filesets>
						<fileset>
							<directory>${basedir}/src/main/resources/static/built</directory>
						</fileset>
					</filesets>
				</configuration>
			</plugin>
		</plugins>
	
Then on the command line run the command:

`./mvnw clean `

The implementation of the alternative can be found on alongside the _/CA2_ directory of my bitbucket repository. 