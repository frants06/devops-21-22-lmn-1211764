# Class Assignment 2 Part 1
## Introduction
 In this assignment we are meant to use gradle as the build tool on a messenger/chat app.
 The building process includes compiling, linking, and packaging the code. Gradle is a general-purpose 
build tool known for its flexibility. A main feature of Gradle is that it includes a rich build
language based on Groovy, opposite to Maven, for instance, that uses XML. Also, Gradle works with tasks, being a task, 
an action that a build performs. We are going to see many examples throughout this report


### Analysis, design and implementation of the requirements
**Step one:** Most of our work will happen on the **build.gradle** file. First, we need to make sure that we have the
necessary plugins and dependencies needed for this project to build. Most of them already come with the repository by default.
The only dependency we had to add was the *JUnit* one.

    plugins {
    id 'java'
    id 'application'
    }
    dependencies {
    // Use Apache Log4J for logging
    implementation group: 'org.apache.logging.log4j', name: 'log4j-api', version: '2.11.2'
    implementation group: 'org.apache.logging.log4j', name: 'log4j-core', version: '2.11.2'
    testImplementation('junit:junit:4.12')
    }

###### The Java Plugin

"The Java plugin adds Java compilation along with testing and bundling capabilities to a project...
<br> By applying the Java Library Plugin, you get a whole host of features:

-A compileJava task that compiles all the Java source files under src/main/java

-A compileTestJava task for source files under src/test/java

-A test task that runs the tests from src/test/java

-A jar task that packages the main compiled classes and resources from src/main/resources into a single JAR named <project>-<version>.jar

-A javadoc task that generates Javadoc for the main classes " (Information found on [Java Plugin](https://docs.gradle.org/current/userguide/java_plugin.html))

###### The Application Plugin

"The Application plugin facilitates creating an executable JVM application. It makes it easy to start the application 
locally during development, and to package the application as a TAR and/or ZIP including operating system specific start 
scripts...
<br>The Application plugin adds the following tasks to the project.

-run — JavaExec
<br/>Depends on: classes
<br/>_Starts the application._

-startScripts — _CreateStartScripts_
<br/>Depends on: jar
<br/>Creates OS specific scripts to run the project as a JVM application.

-installDist — Sync
<br/>Depends on: jar, startScripts
<br/>_Installs the application into a specified directory._

-distZip — Zip
<br/>Depends on: jar, startScripts
<br/>_Creates a full distribution ZIP archive including runtime libraries and OS specific scripts._

-distTar — Tar
<br/>Depends on: jar, startScripts
<br/>_Creates a full distribution TAR archive including runtime libraries and OS specific scripts._"([Application Plugin](https://docs.gradle.org/current/userguide/application_plugin.html))

**Step two:** Create a task to run the server
            
    task runServer(type:JavaExec, dependsOn: classes){

        group = "DevOps"
        description = "Launches the server on localhost 59001"
            
        classpath = sourceSets.main.runtimeClasspath
        mainClass = 'basic_demo.ChatServerApp'
        args '59001'

Making this task ended up being quite simple since we already had a runClient task which is quite similar. The only thing we had 
to change was the _mainClass_ to match with the class responsible for executing the server, the arguments required 
and obviously the description of the task.

**Step three:** Add a simple unit test and update the gradle script so that it is able to execute the
test.

`testImplementation('junit:junit:4.12')`

`./gradlew test`

Since, as we've seen above, the java plugin incorporates a test task that allow us to run all the tests. Hence, the only thing 
we add to do was to add the JUnit dependency corresponding to the version we are using on our project. And the run the task on the command line
using the command shown above.

**Step four:** Add a new task of type Copy to be used to make a backup of the sources of the
application.

    task backup(type:Copy){
        group = "DevOps"
        description = "Copies the contents of the src folder to a new backup folder."
        
        from 'src'
        into 'backup'
    }

This also ended up being quite straight forward with the use of a _Copy_ type task. We define the source directory of files
we want to copy, by writing _from_ following with the address. And then use the word _into_ following with the destination
for the copied files to set the output. If we attribute to the destination a name of a folder that doesn't exist, it will create it automatically.

**Step five:** Add a new task of type Zip to be used to make an archive (i.e., zip file) of the
sources of the application.

    task archive(type:Zip){
        group = "DevOps"
        description = "Copies the contents of the src folder to a new zip file."
        
        from 'src'
        archiveName('srcArchive.zip')
        destinationDir(file('archive'))
        doFirst {
            println 'Creating compressed file of Source contents into \'Archive\' directory'
        }

And finally the _Zip_ task which is similar to the copy task. The main difference is that we need to give a name to the zip
file we're creating and instead of using _into_ to set the output of the created zip file, we need to use _destinationDir_ following
with the output address as seen below. In this task also tested the _doFirst_ line to add an action(printing a line on the console describing what's happening)
for the task to execute before creating the zip file. The opposite to this would be the _doLast_.

