# Class Assignment 5 Part 1
## Introduction
 The goal of this assignment is to create a pipeline with Jenkins using the gradle basic demo from the CA2 part 1.

**Jenkins** is an open source **Continuous Integration** tool. Jenkins allows developers to easily integrate changes in a project
and to automate continuous builds.

_What is Continuous Integration?_

"[Continuous integration](https://aws.amazon.com/devops/continuous-integration/) is a DevOps software development practice where developers regularly merge their code changes 
into a central repository, after which automated builds and tests are run.The key goals of continuous integration are 
to find and address bugs quicker, improve software quality, and reduce the time it takes to validate and release new software updates."

### Analysis, design and implementation of the requirements

Before starting analysing how we implemented the assignment, it is very important to understand the concept of **Pipeline**.

Basically, it is a set of strategies that allows a team of developers to create and efficient and repeatable process to 
develop software.

![Pipeline](PipelineImage.JPG)

"[Jenkins Pipeline](https://www.jenkins.io/doc/book/pipeline/) (or simply "Pipeline" with a capital "P") is a suite of plugins which supports implementing and 
integrating continuous delivery pipelines into Jenkins...

...The definition of a Jenkins Pipeline is written into a text file (called a Jenkinsfile) which in turn can be committed to a project’s source control repository.
 This is the foundation of "Pipeline-as-code"; treating the CD pipeline a part of the application to be versioned and reviewed like any other code...

A Jenkinsfile can be written using two types of syntax - Declarative and Scripted.

Declarative and Scripted Pipelines are constructed fundamentally differently. Declarative Pipeline is a more recent feature of Jenkins Pipeline which:

1. provides richer syntactical features over Scripted Pipeline syntax, and
2. .is designed to make writing and reading Pipeline code easier."

###### Step One - Installing Jenkins
First we needed to install Jenkins. There are several ways to install Jenkins. For instance, either through a docker container or
running directly the _War_ file. For this assignment I chose the second option. After downloading the file, all I had to do 
was executing the command:

`java -jar jenkins.war`

Note: By default Jenkins will use the port 8080. If you want to use a different one you can add the flag: `--httpPort=<Port>`
to the command.

When we execute that command for the first time a setting up page can be accessed on the port we chose where we can choose the
initial plugins we want Jenkins to have. One that will be very useful for this assignment is the **Junit** plugin.

###### Step Two - Creating the Pipeline
To create a Pipeline all we need to do is to select the option _"New Item"_ on the main page and then select _"Pipeline"_.

On the Pipeline configuration we can either put our script there directly, or we can choose the option _"Pipeline script from SCM"_
which will read a Jenkinsfile that exist in a repository. Beyond defining the repository, we also need to define the script path.

Note: If it is a private repository we need to add a pair of credentials so that Jenkins will be able to access the repository.

###### Step Three - Setting up the Jenkinsfile

There are four main concepts important to understand in order to set up a Pipeline:

1. **Job** "A user-configured description of work which Jenkins should perform, such as building a piece of software, etc."
2. **Node** "A machine which is part of the Jenkins environment and capable of executing Pipelines or jobs. Both the Master and Agents are considered to be Nodes."
_Creates a workspace, a directory, where Jenkins builds the project, contains the source code and saves any generated files_
3. **Stage** "stage is part of Pipeline, and used for defining a conceptually distinct subset of the entire Pipeline, for example: "Build", "Test", and "Deploy", which is used by many plugins to visualize or present Jenkins Pipeline status/progress."
4. **Step** "A single task; fundamentally steps tell Jenkins what to do inside of a Pipeline or job."

[Glossary source](https://www.jenkins.io/doc/book/glossary/)

For this assignment we had to define four different **stages**:

1. **Check Out**: Check out the code from the repository.

       stage('Checkout') {
       steps {
       echo 'Checking out...'
       git credentialsId: '1211764-bitbucket-credentials', url: 'https://bitbucket.org/frants06/gradle_basic_demo-1211764'
       }

All we have to do is insert the url of our repository (I forked the repository with the gradle_basic_app) and our bitbucket credentials, in case
the repository is not public. Before executing the git command we also added a descriptive message to appear on the console.

2. **Assemble**: Compiles and Produces the archive files with the application

       stage('Assemble') {
       steps {
       echo 'Assembling...'
       bat './gradlew clean build -x test'
       }
       
This assignment requires us to separate the build from running the tests which means that we couldn't simply use the gradle
build task. So, I haded the flag `-x test` not to run the tests.

3. **Test**: Execute the unit tests and publishes a report on Jenkins.

       stage('Test') {
       steps {
       echo 'Testing...'
       bat './gradlew test'
       junit '**/test-results/**/*.xml'
       }

Similarly to the previous stages, we initially wrote a descriptive message about the stage using the `echo` command.
Then used the `./gradlew test` to run the project's unit test. And finally, we used the jenkins' Junit plugin to publish a report 
with the test results, indicating where the xml report is on the project.

![Tests Report](Tests Report.JPG)

4. **Archive**: Archives in Jenkins the archive files (generated during Assemble)

       stage('Archiving') {
       steps {
       echo 'Archiving...'
       archiveArtifacts 'build/distributions/*'
       }

All we had to do was to use the Jenkins' step archiveArtifacts following with the location of the files we wanted to archive.

Note: If we not sure how any of the steps' sintaxe is or what steps exist, we can use Jenkins Snippet Generator to help with it.

![Snippet Generator](Snippet Generator.JPG)

###### Step Four - Build

Finally, after setting up everything we just need to go to the job's dashboard, and we can build it and check if everything
is working according to the plan.

![Stage View](Stage View.JPG)

![Successful Build](Successful Build.JPG)




