# Class Assignment 5 Part 2
## Introduction
 The goal of this assignment is to create a pipeline with Jenkins using the gradle version of the spring and react tutorial app.


### Analysis, design and implementation of the requirements

This assignment is very similar to the part one except for two new stages we were required to add. For this technical report
I am going to focus on how I generated the Javadoc document and published it on Jenkins and how to build and publish an docker
image with Jenkins. The other stages are exactly the same as the previous exercise but with a different app.

###### **Step One: Javadoc stage**

Gradle's java plugin already as a default task that generates a javadoc so the first thing to do was to add a step to execute
that task.

Then to publish the document on Jenkins we add to install the Jenkins' plugin _**HTML publisher**_. To use the plugin on the Jenkinsfile
we need to use the step `publishHTML` and then define the right settings:

    stage('Javadoc') {
    steps {
    echo 'Publishing...'
    bat  './gradlew javadoc'
    publishHTML (target : [allowMissing: false,
    alwaysLinkToLastBuild: true,
    keepAll: true,
    reportDir: 'build/docs/javadoc',
    reportFiles: 'index.html',
    reportName: 'My Reports',
    reportTitles: 'The Report'])
    }

Options:

1. allowMissing: "If checked, will allow report to be missing and build will not fail on missing report"
2. alwaysLinkToLastBuild: "If this control and "Keep past HTML reports" are checked, publish the link on project level even if build failed."
3. keepAll: "If checked, archive reports for all successful builds, otherwise only the most recent"
4. reportDir: "The path to the HTML report directory relative to the workspace.2
5. reportFiles: "The file(s) to provide links inside the report directory"
6. reportName: "The name of the report to display for the build/project, such as "Code Coverage"""
7. reportTitles: "The optional title(s) for the report files, which will be used as the tab names. If this is not provided, file names will be used instead."

[Source](https://www.jenkins.io/doc/pipeline/steps/htmlpublisher/)


###### **Step One: Build and publish a Docker image**

First, we need to install Jenkins' Docker and Docker Pipeline plugins.

I generated the image through a Dockerfile (the one on the folder _Web_ of the CA4 Part 2 assignment). I placed the file on the same
level as the Jenkinsfile.

On the Jenkinsfile I first defined a few variables:

    environment {
    registry = "francisco1211764/devops-21-22-lmn-1211764"
    registryCredential = "dockerhub"
    dockerimage = ""
    }

_Registry_ is the name of my docker hub repository.

_RegistryCredential_ is the name of a pair of Credentials I added on Jenkins (first I had to generate an access token on Docker Hub)

And _dockerimage_ which is the image that I will be creating later on.

**Building the image:**

    stage('Build Image') {
    steps {
    script {
    echo 'Building Docker Image...'
    dockerImage = docker.build registry + ":${env.BUILD_ID}"
    }

For building the image we need to use the step `docker.build` following with the name of our repository where we will be publishing the image and its tag.
In this example the tag is the ID of the build. We are adding the image to a variable so that we can publish it on the next stage.

**Publishing the image:**

    stage('Publish Image') {
    steps {
    script {
    echo 'Publishing Docker Image...'

                    docker.withRegistry('', registryCredential) {
                    dockerImage.push()
                }
First we use the step `docker.registry` to login into our docker account, and then we use the command push following the name of the variable with the image we created.


### Alternative - Analysis

As an alternative I chose to explore _**TeamCity**_, and I'm using the free trial of Team City cloud.

###### **What is TeamCity**

"TeamCity's slogan "Powerful Continuous Integration out of the box" speaks to what they offer in their solution over and
above a free option like Jenkins: out of the box usability with excellent build history, source control and build chain tools. 
Its slick graphical interface and ease of use features make it more palatable for some new to continuous integration. 
TeamCity runs in a Java environment, usually on an Apache Tomcat server, though it can be installed on both Windows and Linux servers.
TeamCity offers equal support for .NET and openstack projects, integrating into both Visual Studio and IDEs like Eclipse."

[Source](https://www.upguard.com/blog/teamcity-vs-jenkins-for-continuous-integration#toc-1)

###### **Jenkins vs TeamCity**

The main difference I found is that Team City has a very intuitive GUI and you can do pretty much everything through it, 
opposite to Jenkins that we need a script (Jenkinsfile) that can be overwhelming at times. 

On the other hand, Jenkins has a more rich collection of plugins.

"Jenkins is an open source continuous integration tool, while TeamCity is a proprietary offering from JetBrains. TeamCity 
is easier to configure and more straightforward to use, while Jenkins has a rich plugin ecosystem and integrations." 

[Source](https://www.upguard.com/blog/teamcity-vs-jenkins-for-continuous-integration#:~:text=Jenkins%20is%20an%20open%20source,rich%20plugin%20ecosystem%20and%20integrations.)

### Alternative - Implementation

###### **Step One: Creating a project**

Team City has the concept of project instead of Job. But both concepts are identical.

![Project](Alternative.CreateProject.JPG)

As we can see in the image, we don't need a script such as Jenkinsfile, simply insert the repository URL and access token and you can define the steps later on.

![Project](Alternative.CreateProject2.JPG)

###### **Step Two: Defining the steps**

One thing I found harder on team city is to group the steps in a stage and customize them. One benefit of having a script
is that once you surpass the sintaxe learning curve, everything becomes more customizable.

![Project](Alternative.BuildSteps.JPG)

One interesting feature of Team City is that it detects steps you can do based on your repository.

![Project](Alternative.AutoSteps.JPG)

To archive any artifacts instead of creating a step we need to go to the Build general settings and define the path.

![Project](Alternative.Artifacts.JPG)

###### **Step Three: Running a build**

Once everything is set up, all we have to do is to queue a build.

![Project](Alternative.build.JPG)

![Project](Alternative.Build2.JPG)

Note: The build publishes automatically a test report.
